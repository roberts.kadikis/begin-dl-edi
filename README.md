# START HERE
This is a short tutorial on practical deep learning. Includes Anaconda, Python, JupyterLab, TensorFlow and more. 

# ENVIRONMENT
## Conda
This tutorial is written for Ubuntu; however, working in other OS should be very similar. This is one of the reasons we use an environment and package manager Anaconda [[Download here (Python 3.. version)](https://www.anaconda.com/download/#linux)]. The following is a very short installation and use description, for the cleaner and less error-prone Anaconda installation look [here](http://hub.edi.lv/index.php/Tools/Python). 

After the download, install Anaconda using Linux terminal:
```
chmod +x /path/to/yourscript.sh

/path/to/yourscript.sh
```

In the terminal: 
```
conda
```
Either you'll see info about this environment manager, or the command will not be recognized. In later case, add the following line to hidden file *.bash_profile* or *.profile* (those are hidden files in your home folder):
```
PATH=$HOME/anaconda3/bin:$PATH
```
The change will take effect after next login to computer or after terminal command:
```
source ~/path/to/profile/file
```
This will also make your system to use the Anaconda Python, which can be seen with terminal comand:
```
which python
```
Now we can create environments in Anaconda. Each environment will have its own Python version and separate list of installed python modules. Create a new environment called *myenv*. It will have the default python of Anaconda3:
```
conda create --name myenv
```
List your existing environments:
```
conda env list
```
Activate *myenv* evnvironment, so that terminal shows the name of the environment in brackets:
```
source activate myenv
```
To exit the environment:
```
source deactivate
```
While in the active environment, you can check the python of the environment:
```
python
```
The command will open the python interpreter depicted with >>> at the begining of the line. Further comands will be interpreted as python code. You can try to import some python modules:
```
import tensorflow
```
Probably not yet installed. To exit the python interpreter:
```
quit()
```

We want to install the *tensorflow* module. Just google 'conda tensorflow' and one of the first results will lead you to an Anaconda Cloud page with installation commands. If you have a GPU, which is the case if you use EDI HPC server, then search for *tensorflow-gpu* module instead. You'll find a command similar to this one:
```
conda install -c anaconda tensorflow-gpu 
```
When installing, conda will show which dependency modules will be added and which will be upgraded or downgraded. So installation of new modules may affect the working of previously installed modules. Computer vision module opencv is quite delicate and often brakes after installation of other modelues. In such case, you can just reinstall the opencv. Also, be careful not to accidentaly downgrade your python 3 to python 2. Sometimes you will need to install specific versions of the modules, so google will remain your friend. If some modules are not acquirable trough *conda install*, you can use *pip install* while the environment is active. 

To see the modules installed in the active environment:
```
conda list
```
Nice way to use environments is to create a new environment for each of your projects. In this way, it is easier to track and manage different dependencies (python modules, their versions).


## Jupyter
Jupyter notebook and JupyterLab are interactive development platforms, good for experimenting and sharing short codes. We will install JupyterLab in its own conda environment. Create a new environment, for example *jlab*, activate it, and install:
```
conda install -c conda-forge jupyterlab
```
In order for this JupyterLab to be able to use your other conda environments, install a JupyterLab extension (while *jlab* is active):
```
conda install nb_conda_kernels
```
Then install a different jupyter lab extension in the conda environments you want the *jlab* to see. For examle, activate *myenv* environment and:
```
conda install ipykernel
```


To run JupyterLab on your local machine:
```
jupyter lab
````
If you are favored by the software gods today, this will open an internet browser with working JupyterLab and you will be able to choose a python kernel with *myenv* in its name. In JupyterLab you can create jupyter notebooks. These notebooks consist of cells. Code in the code cells can be executed and the resulting prints or images will be displayed below this cell. While the kernel is not restarted, the variables created in one cell are available by the other cells.

 
When working with EDI server, you'll need to run JupyterLab on the server, while interacting with it in your local browser. Assuming that necessary installation was done on the server:
* Connect to the server: `ssh -X username@serveradress`
* Activate conda environment for jupyter: `source activate jlab`
* Start jupyterlab in no-browser mode and choose a port (for example 55555): `jupyter lab --no-browser --port 55555`

Now on your local machine:
* In terminal, do port forwarding (in our example we use 8888 port of the local machine): `ssh -fNL localhost:8888:localhost:55555 username@serveradress`
* Open the internet browser, and in the adress bar go to the chosen local port: `localhost:8888`

Now a JupyterLab should open in your local browser. Here you can load existing notebooks or create your own.


## Tmux
When working with EDI server you might want the possibility to keep your server processes (such as jupyterlab, network training) continue to run even when you close all local terminals conncted to server. A tmux on the server can help with that, but you'll need administrator access to install it:
```
sudo apt install tmux

```
To create new tmux session called *mysession*:
```
tmux new -s mysession
```
Now do your network training or open the jupyterlab while in the tmux session. You will be able to return to this session after you have disconnected and reconnected to the server. To quit but not terminate the session press Ctr+b then press d.
To list your sessions:
```
tmux ls
```
To attach to existing session called *mysession*:
```
tmux a -t mysession
```
To delete existing session (while not inside the session):
```
tmux kill-session -t mysession
```



# MACHINE LEARNING
Further lessons are available in Jupyter notebook form. So you can clone this git repository and load the notebooks in your JupyterLab:
```
git clone http://git.edi.lv/roberts.kadikis/begin-dl.git
```

Or just read them down below and implement the code as you like.

## Data
...

## Convolutional Neural Network
...

## Training
...

## Inference
...

# RESOURCES
Internet has loads of good courses, tutorials, and materials on deep learning. This list is just a small sample.
* [Coursera - Machine Learning](https://www.coursera.org/learn/machine-learning) - basics of machine learning and neural networks. What is linear regression and what is logistic regression? What is cost function, and what is activation function? Overfitting vs bias, when do we need more data or more complex model? And much more.
* [Stanford - Convolutional Neural Networks for Visual Recognition](http://cs231n.stanford.edu) - Must have if dealing with image or video data.


